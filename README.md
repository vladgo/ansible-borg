### Ansible role for Borg backup install

#### Usage

See examples in `tests` folder.

#### Update local borg copy for offline install

```bash
curl -OL https://github.com/borgbackup/borg/releases/download/1.0.10/borg-linux64
```